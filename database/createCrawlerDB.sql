SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE USER 'markos1'@'%' IDENTIFIED BY 'markos1';

GRANT USAGE ON * . * TO 'markos1'@'%' IDENTIFIED BY 'markos1' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;
GRANT USAGE ON * . * TO 'markos1'@'localhost' IDENTIFIED BY 'markos1' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;

CREATE SCHEMA IF NOT EXISTS `markos1` DEFAULT CHARACTER SET utf8 ;

USE `markos1` ;



-- -----------------------------------------------------

-- Table `markos1`.`Source`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `markos1`.`Source` ;



CREATE  TABLE IF NOT EXISTS `markos1`.`Source` (

  `idSource` INT NOT NULL AUTO_INCREMENT ,

  `Name` VARCHAR(200) NOT NULL ,

  `EveryNDays` INT NOT NULL DEFAULT 1 ,

  PRIMARY KEY (`idSource`) ,

  UNIQUE INDEX `Name_UNIQUE` (`Name` ASC) )

ENGINE = InnoDB;







-- -----------------------------------------------------

-- Table `markos1`.`wfState`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `markos1`.`wfState` ;



CREATE  TABLE IF NOT EXISTS `markos1`.`wfState` (

  `idwfState` INT NOT NULL ,

  `name` VARCHAR(100) NOT NULL ,

  `description` VARCHAR(2000) NULL ,

  PRIMARY KEY (`idwfState`) )

ENGINE = InnoDB;





-- -----------------------------------------------------

-- Table `markos1`.`DWBatch`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `markos1`.`DWBatch` ;



CREATE  TABLE IF NOT EXISTS `markos1`.`DWBatch` (
  `idDWBatch` INT NOT NULL AUTO_INCREMENT ,
  `idSource` INT NULL ,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `completed` DATETIME NULL ,
  `integrated` DATETIME NULL ,
  `cancelled` DATETIME NULL ,
  `idWFState` INT NOT NULL ,
  `notes` TEXT NULL ,

  PRIMARY KEY (`idDWBatch`) ,

  INDEX `idSource_idx` (`idSource` ASC) ,

  INDEX `DWBatch_idFWState_idx` (`idWFState` ASC) ,

  CONSTRAINT `idSource`

    FOREIGN KEY (`idSource` )

    REFERENCES `markos1`.`Source` (`idSource` )

    ON DELETE NO ACTION

    ON UPDATE NO ACTION,

  CONSTRAINT `DWBatch_idFWState`

    FOREIGN KEY (`idWFState` )

    REFERENCES `markos1`.`wfState` (`idwfState` )

    ON DELETE NO ACTION

    ON UPDATE NO ACTION)

ENGINE = InnoDB;







-- -----------------------------------------------------

-- Table `markos1`.`Project`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `markos1`.`Project` ;



CREATE  TABLE IF NOT EXISTS `markos1`.`Project` (

  `idProject` INT NOT NULL AUTO_INCREMENT ,

  `Name` VARCHAR(255) NOT NULL ,

  `Homepage` VARCHAR(255) NOT NULL ,

  PRIMARY KEY (`idProject`) ,

  INDEX `IDX_Project_Name` (`Name` ASC) ,

  INDEX `IDX_Project_Homepage` (`Homepage` ASC) )

ENGINE = InnoDB;





-- -----------------------------------------------------

-- Table `markos1`.`Doap`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `markos1`.`Doap` ;



CREATE  TABLE IF NOT EXISTS `markos1`.`Doap` (

  `idDoap` INT NOT NULL AUTO_INCREMENT ,

  `idDWBatch` INT NULL ,

  `Name` VARCHAR(255) NULL ,

  `Shortdesc` VARCHAR(1000) NULL ,

  `Description` VARCHAR(4000) NULL ,

  `Homepage` VARCHAR(255) NULL ,

  `Created` DATE NULL ,

  `Mailing_list` VARCHAR(255) NULL ,

  `Download_page` VARCHAR(255) NULL ,

  `Bug_database` VARCHAR(255) NULL ,

  `Platform` VARCHAR(255) NULL ,

  `Service_endpoint` VARCHAR(255) NULL ,

  `Audience` VARCHAR(255) NULL ,

  `Blog` VARCHAR(255) NULL ,

  `idProject` INT NULL ,

  `modified` BIT NOT NULL DEFAULT b'0' ,

  `old_homepage` TEXT NULL ,

  `category` TEXT NULL ,

  `license` TEXT NULL ,

  `download_mirror` TEXT NULL ,

  `wiki` TEXT NULL ,

  `programming_language` TEXT NULL ,

  `os` TEXT NULL ,

  `language` TEXT NULL ,

  PRIMARY KEY (`idDoap`) ,

  INDEX `idDWBatch_idx` (`idDWBatch` ASC) ,

  INDEX `Doap_Project_idx` (`idProject` ASC) ,

  INDEX `Doap_modified` (`modified` ASC) ,

  CONSTRAINT `idDWBatch`

    FOREIGN KEY (`idDWBatch` )

    REFERENCES `markos1`.`DWBatch` (`idDWBatch` )

    ON DELETE NO ACTION

    ON UPDATE NO ACTION,

  CONSTRAINT `Doap_Project`

    FOREIGN KEY (`idProject` )

    REFERENCES `markos1`.`Project` (`idProject` )

    ON DELETE NO ACTION

    ON UPDATE NO ACTION)

ENGINE = InnoDB;





-- -----------------------------------------------------

-- Table `markos1`.`DoapRepository`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `markos1`.`DoapRepository` ;



CREATE  TABLE IF NOT EXISTS `markos1`.`DoapRepository` (

  `idDoapRepository` INT NOT NULL AUTO_INCREMENT ,

  `Browse` VARCHAR(255) NULL ,

  `Anon_root` VARCHAR(255) NULL ,

  `Location` VARCHAR(255) NULL ,

  `Type` VARCHAR(45) NOT NULL ,

  `idDoap` INT NOT NULL ,

  PRIMARY KEY (`idDoapRepository`) ,

  INDEX `idDoapRepositoryDoap_idx` (`idDoap` ASC) ,

  CONSTRAINT `idDoapRepositoryDoap`

    FOREIGN KEY (`idDoap` )

    REFERENCES `markos1`.`Doap` (`idDoap` )

    ON DELETE NO ACTION

    ON UPDATE NO ACTION)

ENGINE = InnoDB;





-- -----------------------------------------------------

-- Table `markos1`.`DoapVersion`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `markos1`.`DoapVersion` ;



CREATE  TABLE IF NOT EXISTS `markos1`.`DoapVersion` (

  `idDoapVersion` INT NOT NULL AUTO_INCREMENT ,

  `Platform` VARCHAR(255) NULL ,

  `Revision` VARCHAR(255) NULL ,

  `File_release` TEXT NULL ,

  `idDoap` INT NULL ,

  `name` VARCHAR(255) NULL ,

  `created` DATE NULL ,

  PRIMARY KEY (`idDoapVersion`) ,

  INDEX `idDoap_idx` (`idDoap` ASC) ,

  CONSTRAINT `idDoap`

    FOREIGN KEY (`idDoap` )

    REFERENCES `markos1`.`Doap` (`idDoap` )

    ON DELETE NO ACTION

    ON UPDATE NO ACTION)

ENGINE = InnoDB;





-- -----------------------------------------------------

-- Table `markos1`.`FoafPerson`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `markos1`.`FoafPerson` ;



CREATE  TABLE IF NOT EXISTS `markos1`.`FoafPerson` (

  `idFoafPerson` INT NOT NULL AUTO_INCREMENT ,

  `firstName` VARCHAR(45) NULL ,

  `lastName` VARCHAR(45) NULL ,

  PRIMARY KEY (`idFoafPerson`) )

ENGINE = InnoDB;





-- -----------------------------------------------------

-- Table `markos1`.`DoapRole`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `markos1`.`DoapRole` ;



CREATE  TABLE IF NOT EXISTS `markos1`.`DoapRole` (

  `idDoapRole` INT NOT NULL AUTO_INCREMENT ,

  `Name` VARCHAR(45) NOT NULL ,

  PRIMARY KEY (`idDoapRole`) ,

  UNIQUE INDEX `Name_UNIQUE` (`Name` ASC) )

ENGINE = InnoDB;





-- -----------------------------------------------------

-- Table `markos1`.`DoapProjectFoafPerson`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `markos1`.`DoapProjectFoafPerson` ;



CREATE  TABLE IF NOT EXISTS `markos1`.`DoapProjectFoafPerson` (

  `idDoapProject` INT NOT NULL ,

  `idFoafPerson` INT NOT NULL ,

  `idDoapRole` INT NULL ,

  PRIMARY KEY (`idDoapProject`, `idFoafPerson`) ,

  INDEX `idDoapProjectPerson_idx` (`idDoapProject` ASC) ,

  INDEX `idFoafPerson_idx` (`idFoafPerson` ASC) ,

  INDEX `idDoapRole_idx` (`idDoapRole` ASC) ,

  CONSTRAINT `idDoapProjectPerson`

    FOREIGN KEY (`idDoapProject` )

    REFERENCES `markos1`.`Doap` (`idDoap` )

    ON DELETE NO ACTION

    ON UPDATE NO ACTION,

  CONSTRAINT `idFoafPerson`

    FOREIGN KEY (`idFoafPerson` )

    REFERENCES `markos1`.`FoafPerson` (`idFoafPerson` )

    ON DELETE NO ACTION

    ON UPDATE NO ACTION,

  CONSTRAINT `idDoapRole`

    FOREIGN KEY (`idDoapRole` )

    REFERENCES `markos1`.`DoapRole` (`idDoapRole` )

    ON DELETE NO ACTION

    ON UPDATE NO ACTION)

ENGINE = InnoDB;





-- -----------------------------------------------------

-- Table `markos1`.`RAW_Apache_Project`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `markos1`.`RAW_Apache_Project` ;



CREATE  TABLE IF NOT EXISTS `markos1`.`RAW_Apache_Project` (

  `idRAW_Apache_Project` INT NOT NULL AUTO_INCREMENT ,

  `Name` VARCHAR(255) NULL ,

  `Homepage` VARCHAR(255) NULL ,

  `DoapFile` TEXT NULL ,

  `idDWBatch` INT NULL ,

  PRIMARY KEY (`idRAW_Apache_Project`) ,

  INDEX `RAW_Apache_Project_idDWBatch_idx` (`idDWBatch` ASC) ,

  CONSTRAINT `RAW_Apache_Project_idDWBatch`

    FOREIGN KEY (`idDWBatch` )

    REFERENCES `markos1`.`DWBatch` (`idDWBatch` )

    ON DELETE NO ACTION

    ON UPDATE NO ACTION)

ENGINE = InnoDB;





-- -----------------------------------------------------

-- Table `markos1`.`DoapRepositoryType`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `markos1`.`DoapRepositoryType` ;



CREATE  TABLE IF NOT EXISTS `markos1`.`DoapRepositoryType` (

  `idDoapRepositoryType` INT NOT NULL ,

  `Name` VARCHAR(45) NULL ,

  PRIMARY KEY (`idDoapRepositoryType`) )

ENGINE = InnoDB;



USE `markos1` ;





SET SQL_MODE=@OLD_SQL_MODE;

SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;



-- -----------------------------------------------------

-- Data for table `markos1`.`Source`

-- -----------------------------------------------------

START TRANSACTION;

USE `markos1`;

INSERT INTO `markos1`.`Source` (`idSource`, `Name`, `EveryNDays`) VALUES (1, 'Apache', 1);

INSERT INTO `markos1`.`Source` (`idSource`, `Name`, `EveryNDays`) VALUES (2, 'SourceForge', 30);

INSERT INTO `markos1`.`Source` (`idSource`, `Name`, `EveryNDays`) VALUES (3, 'CodePlex', 15);



COMMIT;



-- -----------------------------------------------------

-- Data for table `markos1`.`wfState`

-- -----------------------------------------------------

START TRANSACTION;

USE `markos1`;

INSERT INTO `markos1`.`wfState` (`idwfState`, `name`, `description`) VALUES (1, 'Initial', 'Initial state of any workflow');

INSERT INTO `markos1`.`wfState` (`idwfState`, `name`, `description`) VALUES (2, 'Completed', 'A batch whose download is complete');

INSERT INTO `markos1`.`wfState` (`idwfState`, `name`, `description`) VALUES (3, 'Integrated', 'A batch whose data has been integrated');

INSERT INTO `markos1`.`wfState` (`idwfState`, `name`, `description`) VALUES (4, 'Cancelled', 'A batch whose download has failed');

INSERT INTO `markos1`.`wfState` (`idwfState`, `name`, `description`) VALUES (5, 'Notified', 'A batch whose Integration has been notified to the CA');



COMMIT;



-- -----------------------------------------------------

-- Data for table `markos1`.`DoapRole`

-- -----------------------------------------------------

START TRANSACTION;

USE `markos1`;

INSERT INTO `markos1`.`DoapRole` (`idDoapRole`, `Name`) VALUES (1, 'Maintainer');

INSERT INTO `markos1`.`DoapRole` (`idDoapRole`, `Name`) VALUES (2, 'Developer');

INSERT INTO `markos1`.`DoapRole` (`idDoapRole`, `Name`) VALUES (3, 'Documenter');

INSERT INTO `markos1`.`DoapRole` (`idDoapRole`, `Name`) VALUES (4, 'Tester');

INSERT INTO `markos1`.`DoapRole` (`idDoapRole`, `Name`) VALUES (5, 'Translator');

INSERT INTO `markos1`.`DoapRole` (`idDoapRole`, `Name`) VALUES (6, 'Helper');



COMMIT;



-- -----------------------------------------------------

-- Data for table `markos1`.`DoapRepositoryType`

-- -----------------------------------------------------

START TRANSACTION;

USE `markos1`;

INSERT INTO `markos1`.`DoapRepositoryType` (`idDoapRepositoryType`, `Name`) VALUES (1, 'SVN');

INSERT INTO `markos1`.`DoapRepositoryType` (`idDoapRepositoryType`, `Name`) VALUES (2, 'BK');

INSERT INTO `markos1`.`DoapRepositoryType` (`idDoapRepositoryType`, `Name`) VALUES (3, 'CVS');

INSERT INTO `markos1`.`DoapRepositoryType` (`idDoapRepositoryType`, `Name`) VALUES (4, 'Arch');

INSERT INTO `markos1`.`DoapRepositoryType` (`idDoapRepositoryType`, `Name`) VALUES (5, 'Bazaar');

INSERT INTO `markos1`.`DoapRepositoryType` (`idDoapRepositoryType`, `Name`) VALUES (6, 'Git');

INSERT INTO `markos1`.`DoapRepositoryType` (`idDoapRepositoryType`, `Name`) VALUES (7, 'HG');

INSERT INTO `markos1`.`DoapRepositoryType` (`idDoapRepositoryType`, `Name`) VALUES (8, 'DARCS');



COMMIT;

