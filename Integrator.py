from doapfiend.doaplib import load_graph
from CrawlerDatabase import CrawlerDatabase
from generic_data_fetcher import Sources
from dw_batch import DwBatch
import base64
from doap_project import DoapProject
import json
from json import JSONEncoder
from Utils import Logger


class TrivialJSONEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

class GenericIntegrator(object):
    def __init__(self, id_source):
        Logger.info("Initializing GenericIntegrator for source " + str(id_source))
        self.id_source = id_source
        #I look for the most recent Apache batch that needs to be integrated; it also needs to be more recent than the last integrated one
        # IF there's no integrated batch
        sqlString = "SELECT IF((SELECT max(created) FROM DWBatch WHERE idSource=" + str(id_source) + " AND idWFState=3) IS NULL, "
        #   select the most recent of this source
        sqlString = sqlString + "(SELECT IDDwBatch FROM DWBatch WHERE idSource=" + str(id_source) + " AND created=(SELECT max(created) FROM DWBatch WHERE idSource=" + str(id_source) + " AND idWFState=2)), "
        # else select the most recent of this source only if it is more recent of the latest integrated one
        sqlString = sqlString + "(SELECT IDDwBatch FROM DWBatch WHERE idSource=" + str(id_source) + " AND created=(SELECT max(created) FROM DWBatch WHERE idSource=" + str(id_source) + " AND idWFState=2) AND created>(SELECT max(created) FROM DWBatch WHERE idSource=" + str(id_source) + " AND idWFState=3)))"
        # The above query has this shape because "SELECT max(TIMESTAMP) .." returns the full timestamp while "IF (<TRUE CONDITION>, (SELECT max(TIMESTAMP) ..), <SOMETHING ELSE>)" returns just the year
        id_batch = CrawlerDatabase.select_int(sqlString)
        if not (id_batch is None):
            self.batch = DwBatch()
            self.batch.id_batch = id_batch
            self.batch.id_source = id_source
#            print(self.id_batch)

class ApacheIntegrator(GenericIntegrator):
    id_source = 1
    def __init__(self):
        super(ApacheIntegrator, self).__init__(Sources.Apache)

    def integrate(self):
        try:
            #Load the whole batch (about 200 projects)
            cursor = CrawlerDatabase.execute_cursor("SELECT Name, Homepage, DoapFile FROM RAW_Apache_Project WHERE idDWBatch="+str(self.batch.id_batch))
            results = cursor.fetchall()
            #for each project in the batch
            for record in results:
                Name = record[0]
                Homepage = record[1]
                DoapFile = record[2]
                doap = load_graph(base64.b64decode(DoapFile))
                dp_new = DoapProject()
                dp_new.load_from_doap(doap)
                dp_new.modified = False
                dp_new.id_batch = self.batch.id_batch
                parameters = {
                             'name': dp_new.name,
                             'homepage': dp_new.homepage
                             }

                #Have I already found this project in this or another source?
                sql = "SELECT idProject FROM Project WHERE lower(Name)='" + Name.lower() + "'"
                id_project = CrawlerDatabase.select_natural(sql)
                if not (id_project is None):   #I have found an existing integrated project that I think it's the same
                    dp_new.id_project = id_project
                    #have I found it in other sources?
                    pass    #there are no other sources so far

                    #have I found it in the same source?
                    sql = "SELECT idProject FROM Doap d JOIN DWBatch dwb on d.idDWBatch=dwb.idDWBatch WHERE d.idProject=" + str(id_project) + " AND dwb.idsource=" + str(ApacheIntegrator.id_source)
                    id_old_doap = CrawlerDatabase.select_natural(sql)
                    #TODO I would like to incapsulate the comparison between projects in a DoapProject's method
                    #if the project exists already integrated compare doap and get a list of important differences and a list of trivial ones
                    if not (id_old_doap is None):
                        dp_old = DoapProject()
                        dp_old.load_from_db(id_old_doap)
                        #if there are important differences flag the project as changed
                        for attribute in DoapProject.important_attributes:
                            if (hasattr(dp_old, attribute) and hasattr(dp_new, attribute)):
                                if (TrivialJSONEncoder().encode(getattr(dp_old, attribute)) != TrivialJSONEncoder().encode(getattr(dp_new, attribute))):
                                    # attribute differs
                                    dp_new.modified = True
                            if (hasattr(dp_old, attribute) != hasattr(dp_new, attribute)):
                                # attribute added or removed
                                dp_new.modified = True
                        dp_new.idDoap = dp_old.idDoap
                        #TODO MOVE EVERYTHING IN A SETTER OF A PROPERTY idDoap
                        if hasattr(dp_new, "svn_repository") and (not (dp_new.svn_repository is None)):
                            dp_new.svn_repository.parent_id_doap = dp_new.idDoap
                        if hasattr(dp_new, "hg_repository") and (not (dp_new.hg_repository is None)):
                            dp_new.hg_repository.parent_id_doap = dp_new.idDoap
                        if hasattr(dp_new, "darcs_repository") and (not (dp_new.darcs_repository is None)):
                            dp_new.darcs_repository.parent_id_doap = dp_new.idDoap
                        if hasattr(dp_new, "bzr_repository") and (not (dp_new.bzr_repository is None)):
                            dp_new.bzr_repository.parent_id_doap = dp_new.idDoap
                        if hasattr(dp_new, "arch_repository") and (not (dp_new.arch_repository is None)):
                            dp_new.arch_repository.parent_id_doap = dp_new.idDoap
                        if hasattr(dp_new, "bk_repository") and (not (dp_new.bk_repository is None)):
                            dp_new.bk_repository.parent_id_doap = dp_new.idDoap
                        if hasattr(dp_new, "cvs_repository") and (not (dp_new.cvs_repository is None)):
                            dp_new.cvs_repository.parent_id_doap = dp_new.idDoap
                        if hasattr(dp_new, "git_repository") and (not (dp_new.git_repository is None)):
                            dp_new.git_repository.parent_id_doap = dp_new.idDoap

                    CrawlerDatabase.execute("UPDATE Project SET Name=%(name)s, Homepage=%(homepage)s WHERE idProject=" + str(id_project), parameters)
                else:
                    dp_new.id_project = CrawlerDatabase.execute("INSERT INTO Project (Name, Homepage) VALUES (%(name)s, %(homepage)s)", parameters)
                #save updated doap file or insert if new

                dp_new.save_to_db()
                #TBD if there are important differences save them (is it a requisite? Is it useful?)
            #mark the batch as integrated
            self.batch.integrate()
        except Exception, e:
            print 'Error integrating: %s' % e

class CodePlexIntegrator(GenericIntegrator):
    def __init__(self):
        super(ApacheIntegrator, self).__init__(Sources.Apache)
        if not (self.id_batch is None):
            print(self.id_batch)

    def integrate(self):
        #Shall I split the batch as it is too large?
        pass
