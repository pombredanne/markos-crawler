from CrawlerDatabase import CrawlerDatabase


class DwBatch:
    def __init__(self):
        self.id_batch = 0

    def load(self, id):
        cur = CrawlerDatabase.execute_cursor("SELECT idDWBatch, idSource, created, completed, integrated, cancelled, idWFState FROM DWBatch WHERE idDWBatch="+str(id))
        row = cur.fetchone()
        if not (row is None):
            self.id_batch = id
            self.id_source = row[1]
            self.created = row[2]
            self.completed = row[3]
            self.integrated = row[4]
            self.cancelled = row[5]
            self.id_wf_state = row[6]
        
    def initialize(self, id_source):
        self.id_source = id_source
        self.id_batch = CrawlerDatabase.execute("INSERT into DWBatch (idsource, idWFState) VALUES (" +  str(self.id_source) + ", 1)")  

    def complete(self):
        self.id_batch = CrawlerDatabase.execute("UPDATE DWBatch SET idWFState=2, completed=SYSDATE()  WHERE iddwbatch=" +  str(self.id_batch))  
    
    def cancel(self):
        self.id_batch = CrawlerDatabase.execute("UPDATE DWBatch SET idWFState=4, cancelled=SYSDATE()  WHERE iddwbatch=" +  str(self.id_batch))

    def integrate(self):
        self.id_batch = CrawlerDatabase.execute("UPDATE DWBatch SET idWFState=3, integrated=SYSDATE()  WHERE iddwbatch=" +  str(self.id_batch))

    def notify(self):
        self.id_batch = CrawlerDatabase.execute("UPDATE DWBatch SET idWFState=5  WHERE idDWBatch=" +  str(self.id_batch))

