import logging
import logging.handlers
import base64
import datetime
import ConfigParser
import json
import sys
from json import JSONEncoder

#log levels
#CRITICAL  50
#ERROR     40
#WARNING   30
#INFO      20
#DEBUG     10

class Logger():
    def __init__(self):
        logger = logging.getLogger( ).setLevel(logging.DEBUG)
        logging.basicConfig(format='%(asctime)s %(levelname)8s %(message)s', filename='/tmp/logcrawler.txt', filemode='a')

#        handler = logging.handlers.RotatingFileHandler('/tmp/markos_crawler', maxBytes=1000000, backupCount=5)
        # create formatter
#        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        # add formatter to ch
#        handler.setFormatter(formatter)
#        logger.addHandler(handler)

        logging.info('Initializing logger')

    @staticmethod
    def debug(msg):
        print msg
        logging.debug(msg)
    @staticmethod
    def error(msg):
        print msg
        print sys.exc_type
        print sys.exc_value
        print sys.exc_traceback
        logging.error(msg)
    @staticmethod
    def info(msg):
        print msg
        logging.info(msg)
    @staticmethod
    def critical(msg):
        print msg
        logging.critical(msg)
    @staticmethod
    def fatal(msg):
        print msg
        logging.fatal(msg)
    @staticmethod
    def warn(msg):
        print msg
        logging.warn(msg)
    @staticmethod
    def warning(msg):
        print msg
        logging.warning(msg)
        
        
class StringList():
    separator = " "
    def load_string_list(self, string_list):
        self.string_list = string_list
        self.string_joined = ""
        first = True
        for s in self.string_list:
            if not first:
                self.string_joined += StringList.separator
            self.string_joined += base64.b64encode(s) 
            first = False
        return self
    def load_string_joined(self, string_joined):
        self.string_joined = string_joined
        self.string_list = []
        tmp_list = self.string_joined.split(StringList.separator)
        for s in tmp_list:
            self.string_list.append(base64.b64decode(s))
        return self


class Configuration():
    CAaddress = ''
    MySQLhost = ''
    MySQLuser = ''
    MySQLpasswd = ''
    MySQLdatabase = ''
    sleep_time = ''
    
    def __init__(self):
        Config = ConfigParser.ConfigParser()
        Config.read("/root/markos-crawler/config")  #file 'config' is in the same folder
        Configuration.CAaddress = Config.get("CodeAnalyser", "ip_address")
        Configuration.MySQLhost = Config.get("Database", "MySQLhost")
        Configuration.MySQLuser = Config.get("Database", "MySQLuser")
        Configuration.MySQLpasswd = Config.get("Database", "MySQLpasswd")
        Configuration.MySQLdatabase = Config.get("Database", "MySQLdatabase")
        Configuration.sleep_time = Config.getfloat("General", "sleep_time")

class TrivialJSONEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

class States(object):
    Initial = 1
    Completed = 2
    Integrated = 3
    Cancelled = 4
    Notified = 5

#initialize unique instance of configuration
Configuration()
#initialize the logger unique instance
Logger()
