from Utils import Logger, Configuration, States
from CrawlerDatabase import CrawlerDatabase
from dw_batch import DwBatch
import urllib2, urllib


class Notifier():
    def run(self):
        #are there completed batches not notified?
        path='http://' + Configuration.CAaddress + '/analyzer/notify'
#        path='http://localhost/notify.php' #JUST FOR TEST
        parameters = {
                     'idWFState': States.Integrated
                     }
        cursor = CrawlerDatabase.execute_cursor("SELECT idDWBatch FROM DWBatch WHERE idWFState=%(idWFState)s", parameters)
        results = cursor.fetchall()
        #for each project in the batch
        for raw in results:
            try:
                postdata=[('batchId',str(raw[0]))]
                postdata=urllib.urlencode(postdata)
                req=urllib2.Request(path, postdata)
                req.add_header("Content-type", "application/x-www-form-urlencoded")
                page=urllib2.urlopen(req).read()
                print page
                dwb = DwBatch()
                dwb.load(raw[0])
                dwb.notify()
            except:
                Logger.error("notifying integration of batch " + str(raw[0]) + ". CA may be not listening.")
        #TODO are there searches ?
        pass

Notifier().run()




