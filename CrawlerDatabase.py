import MySQLdb
from Utils import Configuration

class CrawlerDatabase:
    def __init__(self):
        if not (hasattr(self, 'db') and self.db.open):
            CrawlerDatabase.db = MySQLdb.connect(host=Configuration.MySQLhost, user=Configuration.MySQLuser, passwd=Configuration.MySQLpasswd, db=Configuration.MySQLdatabase)
#        else:
            
    @staticmethod
    def execute_cursor(sqlString, parameters=None):
        cur = CrawlerDatabase.db.cursor()
        if (parameters is None):
            cur.execute(sqlString)
        else:
            cur.execute(sqlString, parameters)
        return cur

    @staticmethod
    def select_string(sqlString, parameters=None):
        cur = CrawlerDatabase.db.cursor()
        if (parameters is None):
            cur.execute(sqlString)
        else:
            cur.execute(sqlString, parameters)
        data = cur.fetchone()
        return data[0]
        
    @staticmethod
    def select_int(sqlString, parameters=None):
        cur = CrawlerDatabase.db.cursor()
        if (parameters is None):
            cur.execute(sqlString)
        else:
            cur.execute(sqlString, parameters)
        data = cur.fetchone()
        return data[0]

    @staticmethod
    def select_natural(sqlString, parameters=None):
        cur = CrawlerDatabase.db.cursor()
        if (parameters is None):
            cur.execute(sqlString)
        else:
            cur.execute(sqlString, parameters)
        if cur.rowcount > 0:
            data = cur.fetchone()
            return data[0]
        else:
            return None

    @staticmethod
    def execute(sqlString, parameters=None):
        cur = CrawlerDatabase.db.cursor()
        if (parameters is None):
            cur.execute(sqlString)
        else:
            cur.execute(sqlString, parameters)
        CrawlerDatabase.db.commit()
        return cur.lastrowid

#initialize the database unique instance
CrawlerDatabase()  
