import dw_batch


class GenericDataFetcher(object):
    def __init__(self, id_source, sourceName):
        self.id_source = id_source
        self.sourceName = sourceName 
        self.batch = dw_batch.DwBatch()
        self.batch.id_source = id_source

    def source(self):
        return self.sourceName + ' ' + str(self.id_source)


class Sources(object):
    Apache = 1
    SourceForge = 2
    CodePlex = 3
