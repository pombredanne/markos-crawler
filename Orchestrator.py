import sys
import time

from Utils import Configuration, Logger
from Integrator import ApacheIntegrator
from apache_data_fetcher import ApacheDataFetcher

class Orchestrator():
    def __init__(self):
        while True:
            self.iteration()
            time.sleep(Configuration.sleep_time) #one day

    def iteration(self):
        try:
            a = ApacheDataFetcher()
            a.batch.initialize(1)
            a.run()
            a.batch.complete()
        except Exception, ex:
            print sys.exc_type
            print sys.exc_value
            print sys.exc_traceback
            Logger.error("%e" % ex)
            a.batch.cancel()

        a = ApacheIntegrator()
        if hasattr(a, 'batch') and not (a.batch is None):
            a.integrate()

Orchestrator()
