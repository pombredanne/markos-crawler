#!/bin/bash

#doapfiend --oh doapfiend
#doapfiend -n --oh doapfiend
#doapfiend -Cn --oh doapfiend
#doapfiend -x --oh doapfiend
#doapfiend -Cx --oh doapfiend
#doapfiend -C --oh doapfiend
#doapfiend --html --oh doapfiend

doapfiend -o http://trac.doapspace.org/doapfiend
doapfiend -u examples/doapfiend.rdf
doapfiend -u http://trac.doapspace.org/doapfiend.rdf
doapfiend --sf nut
doapfiend -n --sf nut
doapfiend -Cn --sf nut
doapfiend -x --sf nut
doapfiend -Cx --sf nut
doapfiend -C --sf nut
doapfiend --html --sf nut

doapfiend --fm nut
doapfiend -n --fm nut
doapfiend -Cn --fm nut
doapfiend -x --fm nut
doapfiend -Cx --fm nut
doapfiend -C --fm nut
doapfiend --html --fm nut

doapfiend --py doapfiend
doapfiend -n --py doapfiend
doapfiend -Cn --py doapfiend
doapfiend -x --py doapfiend
doapfiend -Cx --py doapfiend
doapfiend -C --py doapfiend
doapfiend --html --py doapfiend

