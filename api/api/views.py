from cornice import Service
import uuid
import sys

sys.path.append('..')
from doap_project import InformationSource, CrawlerProject, DoapProject
from dw_batch import DwBatch
from CrawlerDatabase import CrawlerDatabase
from Utils import Configuration
from Utils import TrivialJSONEncoder


searchProjects = Service(name='searchProjectsGET', path='searchProjects/{name}', description='run a search and get a UUID to retrieve it')
#projects = Service(name='retrieveNewMetadata', path='/projects/{IdBatch}/{UUID}/{startDate}/{endDate}', description="get projects metadata by batch, UUID, date interval")
projects = Service(name='retrieveNewMetadata', path='/projects/{IdBatch}', description="get projects metadata by batch")
configuration = Service(name='setConfiguration', path='/configuration/{key}', description="get/set configuration parameters")
_VALUES = {}


@projects.get()
def get_value(request):

    listOfProjects = []
    IdBatch = request.matchdict['IdBatch']
    
    dwb = DwBatch()
    #I search the batch and get the source out of it
    dwb.load(IdBatch)  #load from db
    #I create the information source
    parameters = {
                 'idSource': dwb.id_source
                 } 
    info_source = InformationSource(CrawlerDatabase.select_string("SELECT Name FROM Source WHERE idSource=%(idSource)s", parameters))

    #I search all the doaps in it and add the information source to them
    parameters = {
                 'idDWBatch': dwb.id_batch
                 } 
    cursor = CrawlerDatabase.execute_cursor("SELECT d.idDoap, p.idProject, p.Name, p.Homepage FROM Doap d JOIN Project p ON d.idProject=p.idProject WHERE idDWBatch=%(idDWBatch)s", parameters)
    results = cursor.fetchall()
    for record in results:
        cp = CrawlerProject()
        cp.id_project = record[1]
        cp.name = record[2]
        cp.homepage = record[3]
        #I add to cp the doap
        dp = DoapProject()
        print record[0]
        dp.load_from_db(record[0])
        dp.source = info_source
        cp.doap.append(dp)
        #POSTPONE I search all the doaps in the same project from a different source
        listOfProjects.append(cp)
    a = TrivialJSONEncoder().encode(listOfProjects)
    return a;
