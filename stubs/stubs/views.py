""" Cornice services.
"""
import uuid
import json
from json import JSONEncoder
from cornice import Service



class TrivialJSONEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

class InformationSource:
    def __init__(self, name):
        self.name = name

class CrawlerProject:
    """Contains a list of projects in DOAP format"""
    def __init__(self):
        self.doap = []
    
class DoapProject:
    def __init__(self):
        self.doap = []
        self.old_homepage = []
        self.release = []
        self.category = []
        self.license = []
        self.download_mirror = []
        self.wiki = []
        self.developer = []
        self.language = []
        self.programming_language = []
        self.os = []

class FoafPerson:
    def __init__(self, firstName, lastName):
        self.firstName = firstName
        self.lastName = lastName

class DoapVersion:
    def __init__(self):
        self.os = []

class DoapRepository:
    def __init__(self):
        """"""

searchProjects = Service(name='searchProjectsGET', path='searchProjects/{name}', description='run a search and get a UUID to retrieve it')
projects = Service(name='retrieveNewMetadata', path='/projects/{IdBatch}/{UUID}/{startDate}/{endDate}', description="get projects metadata by batch, UUID, date")
configuration = Service(name='setConfiguration', path='/configuration/{key}', description="get/set configuration parameters")
_VALUES = {}


@searchProjects.get()
def get_value(request):
    return str(uuid.uuid4())

@projects.get()
def get_value(request):

    listOfProjects = []
    IdBatch = request.matchdict['IdBatch']
    UUID = request.matchdict['UUID']
    startDate = request.matchdict['startDate']
    endDate = request.matchdict['endDate']
#    doap = load_graph(fetch_doap('http://svn.apache.org/repos/asf/httpd/site/trunk/content/doap.rdf')) 

    ohloh = InformationSource("ohloh")
    flossmole = InformationSource("flossmole")
    developer = FoafPerson("Alan", "Turing")

    dv = DoapVersion()
    dv.os.append("Ubuntu")
    dv.os.append("Red Hat")
    dv.platform = "platformmmm"
    dv.revision = "reviosionnn"
    dv.file_release = "file releaseee"

    git = DoapRepository()
    git.location = "http://svn.apache.org/repos/asf/abdera"

    dp = DoapProject()
    dp.name = "Apache Abdera"
    dp.shortdesc = "An open source Atom implementation"
    dp.description = "The goal of the Apache Abdera project is to build a functionally-complete, high-performance implementation of the IETF Atom Syndication Format (RFC 4287) and Atom Publishing Protocol (RFC 5023) specifications."
    dp.homepage = "http://abdera.apache.org"
    dp.old_homepage.append("http://abdera.org")
    dp.created = "2008-12-25"
    dp.GitRepository = git
    dp.mailing_list = "http://abdera.apache.org/project.html#lists"
    dp.category.append("http://projects.apache.org/category/xml")
    dp.release.append(dv)

    dp.license.append("http://usefulinc.com/doap/licenses/asl20")
    dp.download_page = "http://abdera.apache.org/#downloads"
    dp.download_mirror.append("http://abdera.apache.org/#mirrors")
    dp.wiki.append("http://abdera.apache.org/wiki")
    dp.bug_database = "https://issues.apache.org/jira/browse/ABDERA"
    dp.developer.append(developer)
    dp.programming_language.append("Java")
    dp.os.append("os1")
    dp.os.append("os2")
    dp.platform = "platformmmmm"
    dp.service_endpoint = "service_endpointttt"
    dp.language.append("English")
    dp.language.append("Italian")
    dp.audience = "audience"
    dp.blog = "http://abdera.apache.org/blog"
    dp.source = ohloh

    cp = CrawlerProject()
    cp.doap.append(dp)
    listOfProjects.append(cp)
    return TrivialJSONEncoder().encode(listOfProjects);



@configuration.post()
def set_value(request):
    key = request.matchdict['key']
    try:
        _VALUES[key] = json.loads(request.body)
    except ValueError:
        return False
    return True

@configuration.get()
def get_value(request):
    key = request.matchdict['key']
    return _VALUES.get(key)

