from doapfiend.doaplib import fetch_doap, load_graph
import generic_data_fetcher
from generic_data_fetcher import Sources
import urllib2
from xml.dom import minidom
from CrawlerDatabase import CrawlerDatabase
import base64
from Utils import Logger

class ApacheDataFetcher(generic_data_fetcher.GenericDataFetcher):
    def __init__(self):
        super(ApacheDataFetcher, self).__init__(Sources.Apache, "Apache")
    def run(self):
        #reading the list of doap files
        response = urllib2.urlopen('https://svn.apache.org/repos/asf/infrastructure/site-tools/trunk/projects/files.xml')
        xml = response.read()
        xmldoc = minidom.parseString(xml)
        itemlist = xmldoc.getElementsByTagName('location')
        
        # region test
        if False:
            doapurl = urllib2.urlopen("http://svn.apache.org/repos/asf/uima/site/trunk/uima-website/docs/doap/uima.rdf")
            xml = doapurl.read()
            doap = load_graph(xml)
        # end region test
        
        # reading 
        for s in itemlist :
#            print s.firstChild.data
            try:
                # getting the rdf-xml from the url
                doapurl = urllib2.urlopen(s.firstChild.data)
                xml = doapurl.read()
                # parsing the rdf
                doap = load_graph(xml)
                # creating ApacheDoap also saves it to the RAW database
                ApacheDoap(doap, xml, self.batch.id_batch)
                Logger.info("Read " + doap.name + " from " + s.firstChild.data)
            except Exception:
                Logger.error('Error loading doap: ' + s.firstChild.data)
            
            
            
#    def completed(self):
        #nothing
#        pass
#    def cancelled(self):
        #nothing
#        pass

class ApacheDoap():
    def __init__(self, doap, xml, idBatch):
        CrawlerDatabase.execute("INSERT into RAW_Apache_Project (name, homepage, doapfile, iddwbatch) VALUES ('" +  doap.name + "', '" +  str(doap.homepage) + "', '" +  base64.b64encode(xml) + "', " + str(idBatch) + ")")

