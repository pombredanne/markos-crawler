#from doapfiend.doaplib import doap
from CrawlerDatabase import CrawlerDatabase
from Utils import Logger, StringList
import sys

class FoafPerson:
    def __init__(self, firstName, lastName):
        self.firstName = firstName
        self.lastName = lastName

class DoapVersion:
    def __init__(self):
        self.os = []

class DoapRepository:
    def __init__(self, parent):
        self.parent_id_doap = parent.idDoap
    def insert_or_update(self):
        parameters = {
                     'type': self.type,
                     'idDoap': self.parent_id_doap
                     }
        id = CrawlerDatabase.select_natural("SELECT idDoapRepository FROM DoapRepository WHERE idDoap=%(idDoap)s AND Type=%(type)s ", parameters)
        parameters = {
                     'location': "" if ((not hasattr(self, "location")) or self.location is None) else self.location,
                     'browse': "" if ((not hasattr(self, "browse")) or self.browse is None) else self.browse,
                     'anon_root': "" if ((not hasattr(self, "anon_root")) or self.anon_root is None) else self.anon_root,
                     'type': self.type,
                     'idDoap': self.parent_id_doap
                     }
        if id is None:
            sql = "INSERT INTO DoapRepository (location, browse, anon_root, type, idDoap) VALUES (%(location)s, %(browse)s, %(anon_root)s, %(type)s, %(idDoap)s)"
        else:
            sql = "UPDATE DoapRepository SET location=%(location)s, browse=%(browse)s, anon_root=%(anon_root)s WHERE idDoap=%(idDoap)s AND Type=%(type)s"
        CrawlerDatabase.execute(sql, parameters)
            
        
class InformationSource:
    def __init__(self, name):
        self.name = name

class CrawlerProject:
    """Contains a list of projects in DOAP format"""
    def __init__(self):
        self.doap = []

class DoapProject:
#   $important attributes is a list of attributes which, if modified, will set the changed flag
    important_attributes = ["homepage", "svn_repository"]  
#     for x in DoapProject.important_attributes
    def __init__(self):
        pass
    def load_from_doap(self, par_doap):
        self.idDoap = None
        self.name = par_doap.name
        self.shortdesc = ("" if (len(par_doap.shortdesc)==0) else par_doap.shortdesc[0])
        self.description = ("" if (len(par_doap.description)==0) else par_doap.description[0])
        self.homepage = "" if (par_doap.homepage is None) else par_doap.homepage.resUri
        self.old_homepage = []
        if len(par_doap.old_homepage) > 0:
            for ohp in par_doap.old_homepage:
                self.old_homepage.append(ohp)
        self.created = par_doap.created
        #svn
        if hasattr(par_doap, "svn_repository") and (not (par_doap.svn_repository is None)):
            r = par_doap.svn_repository
            repo = DoapRepository(self)
            repo.location = "" if ((not hasattr(r, "location")) or r.location is None) else r.location.resUri
            repo.browse = "" if ((not hasattr(r, "svn_browse")) or r.svn_browse is None) else r.svn_browse.resUri
            repo.anon_root = "" if ((not hasattr(r, "anon_root")) or r.anon_root is None) else r.anon_root.resUri
            repo.type = "svn" 
            self.svn_repository = repo
        #bk BitKeeper
        if hasattr(par_doap, "bk_repository") and (not (par_doap.bk_repository is None)):
            r = par_doap.bk_repository
            repo = DoapRepository(self)
            repo.location = "" if ((not hasattr(r, "location")) or r.location is None) else r.location.resUri
            repo.browse = "" if ((not hasattr(r, "bk_browse")) or r.bk_browse is None) else r.bk_browse.resUri
            repo.anon_root = "" if ((not hasattr(r, "anon_root")) or r.anon_root is None) else r.anon_root.resUri
            repo.type = "bk"
            self.bk_repository = repo
        #cvs_repository
        if hasattr(par_doap, "cvs_repository") and (not (par_doap.cvs_repository is None)):
            r = par_doap.cvs_repository
            repo = DoapRepository(self)
            repo.location = "" if ((not hasattr(r, "location")) or r.location is None) else r.location.resUri
            repo.browse = "" if ((not hasattr(r, "cvs_browse")) or r.cvs_browse is None) else r.cvs_browse.resUri
            repo.anon_root = "" if ((not hasattr(r, "anon_root")) or r.anon_root is None) else r.anon_root.resUri
            repo.type = "cvs"
            self.cvs_repository = repo
        #arch
        if hasattr(par_doap, "arch_repository") and (not (par_doap.arch_repository is None)):
            r = par_doap.arch_repository
            repo = DoapRepository(self)
            repo.location = "" if ((not hasattr(r, "location")) or r.location is None) else r.location.resUri
            repo.browse = "" if ((not hasattr(r, "arch_browse")) or r.arch_browse is None) else r.arch_browse.resUri
            repo.anon_root = "" if ((not hasattr(r, "anon_root")) or r.anon_root is None) else r.anon_root.resUri
            repo.type = "arch"
            self.arch_repository = repo
        #bzr Bazaar
        if hasattr(par_doap, "bzr_repository") and (not (par_doap.bzr_repository is None)):
            r = par_doap.bzr_repository
            repo = DoapRepository(self)
            repo.location = "" if ((not hasattr(r, "location")) or r.location is None) else r.location.resUri
            repo.browse = "" if ((not hasattr(r, "bzr_browse")) or r.bzr_browse is None) else r.bzr_browse.resUri
            repo.anon_root = "" if ((not hasattr(r, "anon_root")) or r.anon_root is None) else r.anon_root.resUri
            repo.type = "bzr"
            self.bzr_repository = repo
        #git
        if hasattr(par_doap, "git_repository") and (not (par_doap.git_repository is None)):
            r = par_doap.git_repository
            repo = DoapRepository(self)
            repo.location = "" if ((not hasattr(r, "location")) or r.location is None) else r.location.resUri
            repo.browse = "" if ((not hasattr(r, "git_browse")) or r.git_browse is None) else r.git_browse.resUri
            repo.anon_root = "" if ((not hasattr(r, "anon_root")) or r.anon_root is None) else r.anon_root.resUri
            repo.type = "git"
            self.git_repository = repo
        #hg Mercurial
        if hasattr(par_doap, "hg_repository") and (not (par_doap.hg_repository is None)):
            r = par_doap.hg_repository
            repo = DoapRepository(self)
            repo.location = "" if ((not hasattr(r, "location")) or r.location is None) else r.location.resUri
            repo.browse = "" if ((not hasattr(r, "hg_browse")) or r.hg_browse is None) else r.hg_browse.resUri
            repo.anon_root = "" if ((not hasattr(r, "anon_root")) or r.anon_root is None) else r.anon_root.resUri
            repo.type = "hg"
            self.hg_repository = repo
        #darcs
        if hasattr(par_doap, "darcs_repository") and (not (par_doap.darcs_repository is None)):
            r = par_doap.darcs_repository
            repo = DoapRepository(self)
            repo.location = "" if ((not hasattr(r, "location")) or r.location is None) else r.location.resUri
            repo.browse = "" if ((not hasattr(r, "darcs_browse")) or r.darcs_browse is None) else r.darcs_browse.resUri
            repo.anon_root = "" if ((not hasattr(r, "anon_root")) or r.anon_root is None) else r.anon_root.resUri
            repo.type = "darcs"
            self.darcs_repository = repo
        self.mailing_list = "" #TODO   par_doap.mailing_list.resUri 
        self.category = []
        for ct in par_doap.category:
            self.category.append(ct.resUri)
        self.release = []
        if not (par_doap.releases is None):
            for dr in par_doap.releases:
                dv = DoapVersion()
                dv.name = dr.name
                dv.created = dr.created
                dv.revision = dr.revision
                if not (dr.file_releases is None):
                    dv.file_release = []
                    for fr in dr.file_releases:
                        dv.file_release.append(fr)
                self.release.append(dv)
        self.license = []
        for l in par_doap.license:
            self.license.append(l.resUri)
        self.download_page = "" if (par_doap.download_page is None) else par_doap.download_page.resUri
        self.download_mirror = []
        if len(par_doap.download_mirror) > 0:
            for dl in par_doap.download_mirror:
                self.download_mirror.append(dl.resUri)
        self.wiki = []
        if len(par_doap.wiki) > 0:
            for w in par_doap.wiki:
                self.wiki.append(w.resUri)
        self.bug_database = "" if (par_doap.bug_database is None) else par_doap.bug_database.resUri
        self.developer = []
        self.programming_language = []
        for pl in par_doap.programming_language:
            self.programming_language.append(pl)
        self.os = []
        self.platform = ""
        self.service_endpoint = ""
        self.language = []
        self.audience = ""
        self.blog = ""
        
    def load_from_db(self, idDoap):
        self.idDoap = idDoap
        try:
            cursor = CrawlerDatabase.execute_cursor("SELECT name, shortdesc, description, homepage, created, mailing_list, download_page, bug_database, platform, service_endpoint, audience, blog, modified, old_homepage, category, license, download_mirror, wiki, programming_language, os, language, idDWBatch, idProject FROM Doap WHERE idDoap="+str(idDoap))
            result = cursor.fetchone()
            #for each project in the batch
            if (result is None):
                #throw
                pass
            else:
                self.name = result[0]
                self.shortdesc = result[1]
                self.description = result[2]
                self.homepage = result[3]
                self.created = str(result[4])
                self.mailing_list = result[5]
                self.download_page = result[6]
                self.bug_database = result[7]
                self.platform = result[8]
                self.service_endpoint = result[9]
                self.audience = result[10]
                self.blog = result[11]
                self.modified = True if result[12] == b'\x00' else False #NON FUNZIONA
                self.old_homepage = StringList().load_string_joined(result[13]).string_list
                self.category = StringList().load_string_joined(result[14]).string_list
                self.license = StringList().load_string_joined(result[15]).string_list
                self.download_mirror = StringList().load_string_joined(result[16]).string_list
                self.wiki = StringList().load_string_joined(result[17]).string_list
                self.programming_language = StringList().load_string_joined(result[18]).string_list
                self.os = StringList().load_string_joined(result[19]).string_list
                self.language = StringList().load_string_joined(result[20]).string_list
                self.idDWBatch = result[21]
                self.idProject = result[22] 
                #DoapVersion Table
                self.release = []
                cur = CrawlerDatabase.execute_cursor("SELECT platform, revision, file_release, created, name FROM DoapVersion WHERE idDoap=" + str(self.idDoap))
                results = cur.fetchall()
                for record in results:
                    dv = DoapVersion()
                    dv.name = record[4]
                    dv.created = str(record[3])
                    dv.revision = record[1]
                    dv.platform = record[0]
                    dv.file_release = StringList().load_string_joined(record[2]).string_list  
                    self.release.append(dv)
                #DoapRepository Table 
                cur = CrawlerDatabase.execute_cursor("SELECT browse, anon_root, location, type FROM DoapRepository WHERE idDoap=" + str(self.idDoap))
                results = cur.fetchall()
                for record in results:
                    dr = DoapRepository(self)
                    dr.browse = record[0]
                    dr.anon_root = record[1]
                    dr.location = record[2]
                    dr.type = record[3]
                    if dr.type == 'svn':
                        self.svn_repository = dr
                    if dr.type == 'bk':
                        self.bk_repository = dr
                    if dr.type == 'cvs':
                        pass
#PATCH doapfiend adds a cvs even if it is not there                        self.cvs_repository = dr
                    if dr.type == 'arch':
                        self.arch_repository = dr
                    if dr.type == 'bzr':
                        self.bzr_repository = dr
                    if dr.type == 'git':
                        self.git_repository = dr
                    if dr.type == 'hg':
                        self.hg_repository = dr
                    if dr.type == 'darcs':
                        self.darcs_repository = dr
                """   
                self.developer = []
                """                
        except Exception, e:
            print sys.exc_type
            print sys.exc_value
            print sys.exc_traceback
            Logger.error("%e" % e)

    def save_to_db(self):
        parameters = {
                     'idDWBatch': self.id_batch,
                     'idProject': self.id_project,
                     'name': self.name.encode('utf-8'),
                     'shortdesc': self.shortdesc.encode('utf-8'),
                     'description': self.description.encode('utf-8'),
                     'homepage': self.homepage.encode('utf-8'),
                     'created': self.created,      # format: 2006-03-27
                     'mailing_list': self.mailing_list.encode('utf-8'),
                     'download_page': self.download_page.encode('utf-8'),
                     'bug_database': self.bug_database.encode('utf-8'),
                     'platform': self.platform.encode('utf-8'),
                     'service_endpoint': self.service_endpoint.encode('utf-8'),
                     'audience': self.audience.encode('utf-8'),
                     'blog': self.blog.encode('utf-8'),
                     'old_homepage': StringList().load_string_list(self.old_homepage).string_joined, 
                     'category': StringList().load_string_list(self.category).string_joined,
                     'license': StringList().load_string_list(self.license).string_joined,
                     'download_mirror': StringList().load_string_list(self.download_mirror).string_joined,
                     'wiki': StringList().load_string_list(self.wiki).string_joined,
                     'programming_language': StringList().load_string_list(self.programming_language).string_joined,
                     'os': StringList().load_string_list(self.os).string_joined,
                     'language': StringList().load_string_list(self.language).string_joined
                     }
        try:
            # If "idDoap is None" inserts else updates
            if ((not hasattr(self, 'idDoap')) or self.idDoap is None):  #insert
                sqlString = "INSERT INTO Doap (idDWBatch, idProject, name, shortdesc, description, homepage, created, mailing_list, download_page, bug_database, platform, service_endpoint, audience, blog, modified, old_homepage, category, license, download_mirror, wiki, programming_language, os, language) "
                sqlString += "VALUES (%(idDWBatch)s, %(idProject)s, %(name)s, %(shortdesc)s, %(description)s, %(homepage)s, %(created)s, %(mailing_list)s, %(download_page)s, %(bug_database)s, %(platform)s, %(service_endpoint)s, %(audience)s, %(blog)s, b'" + ("1" if self.modified else "0") + "', %(old_homepage)s, %(category)s, %(license)s, %(download_mirror)s, %(wiki)s, %(programming_language)s, %(os)s, %(language)s)"
                self.idDoap = CrawlerDatabase.execute(sqlString, parameters)
            else:  #update
                sqlString = "UPDATE Doap SET idDWBatch=%(idDWBatch)s, idProject=%(idProject)s, name=%(name)s, shortdesc=%(shortdesc)s, description=%(description)s, homepage=%(homepage)s, created=%(created)s, mailing_list=%(mailing_list)s, download_page=%(download_page)s, bug_database=%(bug_database)s, platform=%(platform)s, service_endpoint=%(service_endpoint)s, audience=%(audience)s, blog=%(blog)s, modified=b'" + ("1" if self.modified else "0") + "', old_homepage=%(old_homepage)s, category=%(category)s, license=%(license)s, download_mirror=%(download_mirror)s, wiki=%(wiki)s, programming_language=%(programming_language)s, os=%(os)s, language=%(language)s " 
                sqlString += "WHERE idDoap="+str(self.idDoap)
                CrawlerDatabase.execute(sqlString, parameters)
        except Exception as ex:
            print sys.exc_type
            print sys.exc_value
            Logger.error(' saving to db: ' + self.name)

        try:
            #delete all releases (I have no way to determine whether it's an update of a record)
            CrawlerDatabase.execute("DELETE FROM DoapVersion WHERE idDoap=" + str(self.idDoap))
            #DoapVersion / releases table
            #insert all releases
            for dv in self.release:  #dv = doap version
                parameters = {
                             'platform': "", #TODO ADD  dv.platform,
                             'revision': dv.revision,
                             'file_release': StringList().load_string_list(dv.file_release).string_joined,
                             'idDoap': self.idDoap,
                             'created': dv.created,      #non standard; found in Apache
                             'name': dv.name            #non standard; found in Apache
                             }
                CrawlerDatabase.execute("INSERT INTO DoapVersion (platform, revision, file_release, idDoap, created, name) VALUES (%(platform)s, %(revision)s, %(file_release)s, %(idDoap)s, %(created)s, %(name)s)", parameters)
        except Exception as ex:
            print sys.exc_type
            print sys.exc_value
            Logger.error(' saving DoapVersion for: ' + self.name)

        try:
            #DoapRepository Table; each project can have just one repository of a specific type
            #insert or update all repositories
            if hasattr(self, "svn_repository") and (not (self.svn_repository is None)):
                self.svn_repository.insert_or_update()
            if hasattr(self, "hg_repository") and (not (self.hg_repository is None)):
                self.hg_repository.insert_or_update()
            if hasattr(self, "darcs_repository") and (not (self.darcs_repository is None)):
                self.darcs_repository.insert_or_update()
            if hasattr(self, "bzr_repository") and (not (self.bzr_repository is None)):
                self.bzr_repository.insert_or_update()
            if hasattr(self, "arch_repository") and (not (self.arch_repository is None)):
                self.arch_repository.insert_or_update()
            if hasattr(self, "bk_repository") and (not (self.bk_repository is None)):
                self.bk_repository.insert_or_update()
            if hasattr(self, "cvs_repository") and (not (self.cvs_repository is None)):
                self.cvs_repository.insert_or_update()
            if hasattr(self, "git_repository") and (not (self.git_repository is None)):
                self.git_repository.insert_or_update()

            
            self.developer = []
            
        except Exception as ex:
            print sys.exc_type
            print sys.exc_value
            Logger.error(' saving DoapRepository for: ' + self.name)

